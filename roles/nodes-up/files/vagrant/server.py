
import socket 
def app(environ, start_response):
    """Simplest possible application object"""
    host_name = socket.gethostname() 
    host_ip = socket.gethostbyname(host_name)                 
    data = bytes('<br><center><h1>Hello JFrog team! <br> host:'+host_name+"</h1></center>", encoding='utf-8')
    status = '200 OK'
    response_headers = [
        ('Content-type', 'text/html'),
        ('Content-Length', str(len(data)))
    ]
    start_response(status, response_headers)
    return iter([data])
# HA website - JFrog 

## Prerequisite

docker , ansible , virtualbox , vagrant 

## Deploy 

* node_count - number of vm machine serving the website.

* ansible-pull -f -e node_count=2 -C master -i ./hosts -d ./ -U https://ranmizrachi@bitbucket.org/ranmizrachi/jfrog-ha-website.git --tags nodes-up,loadbalancer

### Web site

* Loadbalancer (nginx): http://127.0.0.1:90/

- Network:   192.168.78.0/24 
- HostMin:   192.168.78.21 
- HostMax:   192.168.78.254

* Web sites (nodes) http://192.168.78.[20 + node_count]/

## Scale up 

* ansible-playbook local.yml --extra-vars "node_count=2" --tags nodes-up,loadbalancer

## Scale down 

* ansible-playbook local.yml --tags nodes-destroy,loadbalancer --extra-vars "node_count=1"

## Re-deploy

* ansible-playbook local.yml --extra-vars "node_count=2"

## Ansible roles

* nodes-up - spin vm with vagrant 

* loadbalancer - init/update containerized nginx proxy

* nodes-destroy - destory vm


 